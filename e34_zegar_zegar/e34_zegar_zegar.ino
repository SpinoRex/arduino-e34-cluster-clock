#include <Tone.h>


#define RPM_PIN 23
#define SPEED_PIN 22
#define MPG_PIN 25
#define FUEL_PIN 24
#define TEMP_PIN 26

#define HOUR_BTN 41
#define MINUTE_BTN 40

#define ARDUINO_TONE_TIMER0_MIN_FREQ 1
#define ARDUINO_TONE_TIMER1_MIN_FREQ 1

bool hour_btn_pressed = false;
bool minute_btn_pressed = false;

int hours = 0;
int minutes = 0;
int seconds = 0;

float hours_numeric = 0;
float minutes_numeric = 0;

unsigned long clock_previous_millis = 0;
unsigned long clock_current_millis = 0;
int clock_millis_threshold = 1000;

unsigned long buttons_previous_millis = 0;
unsigned long buttons_current_millis = 0;
int buttons_millis_threshold = 250;

float speed_freq = 0;
float rpm_freq = 0;

//Defining tones --> Order defines which timer will be used. Arduino uno has 3 timers: T0 8bit, T1 16bit and T2 8bit. Mega has also T3, T4 and T5, all 16 bit. 8bit timers cannot go below 31Hz so for speedo we need i.e. T1.
//This library is Tone by Brett Hagman (https://github.com/bhagman/Tone)
//For Uno, order is: T2, T1, T0
//For Mega: T2, T3, T4, T5, T1, T0
//-------
//This library does not support ATmega256, only ATmega1280. Since this is the same chip but with different memory size, we can use Mega2560 with it but with small modification:
//Go to %Documents%/Arduino/libraries/Tone-1.0.0 and modify Tone.cpp. Replace every occurence of "#if defined(__AVR_ATmega1280__)" with "#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)" to treat 2560 just like 1280. 
//Without that, every unsupported type is treated like ATmega328 (Uno) so we will have only two timers (0 should not be used!!!)
Tone mpg_tone;  //T2 - 8 bit on Uno/Mega, disables pins 9 and 10
Tone rpm_tone;  //T3 - 16 bit on Mega only, disables pins 2, 3 and 5
Tone speed_tone;  //T4 - 16 bit on Mega only, disables pins 6, 7 and 8
//T5 - 16 bit on Mega only, disables pins 44, 45 and 46
//T1 - 16 bit on Uno/Mega, disables pins 11 and 12
//T0 - 8 bit on Uno/Mega, disables pins 4 and 13



void setup() {
  // put your setup code here, to run once:

  pinMode(RPM_PIN, OUTPUT);
  pinMode(SPEED_PIN, OUTPUT);
  pinMode(MPG_PIN, OUTPUT);
  pinMode(FUEL_PIN, OUTPUT);
  pinMode(TEMP_PIN, OUTPUT);

  pinMode(HOUR_BTN, INPUT);
  pinMode(MINUTE_BTN, INPUT);

  Serial.begin(9600);

  mpg_tone.begin(MPG_PIN);
  rpm_tone.begin(RPM_PIN);
  speed_tone.begin(SPEED_PIN);
}



void clockTick() {
  incSeconds();

  minutes_numeric = (float)minutes + ((float)seconds / 60);
  hours_numeric = (float)hours + ((float)minutes / 60) + ((float)seconds / 3600);
}



void incHours() {
  hours++;

  if(hours == 24)
  {
    hours = 0;
  }
}



void incMinutes() {
  minutes++;

  if(minutes == 60)
  {
    minutes = 0;
    incHours();
  }
}



void incSeconds() {
  seconds++;

  if(seconds == 60)
  {
    seconds = 0;
    incMinutes();
  }
}



void loop() {
  
  //Clock step
  clock_current_millis = millis();
  if(clock_current_millis - clock_previous_millis >= clock_millis_threshold)
  {
    clock_previous_millis = clock_previous_millis + clock_millis_threshold;

    clockTick();

    Serial.print("Time: ");
    Serial.print(hours);
    Serial.print(":");
    Serial.print(minutes);
    Serial.print(":");
    Serial.print(seconds);
    Serial.println();
    Serial.print("Time numeric: ");
    Serial.print(hours_numeric);
    Serial.print("___");
    Serial.print(minutes_numeric);
    Serial.println();

    //Show hours on speedometer
    //speed_freq = round((0.0002 * 10 * hours_numeric * 10 * hours_numeric) + (1.1864 * 10 * hours_numeric) - 3.218);  //E34 525tds
    speed_freq = round((0.0002 * 10 * hours_numeric * 10 * hours_numeric) + (1.2233 * 10 * hours_numeric) - 0.9867);  //E36 318i
    if(speed_freq < ARDUINO_TONE_TIMER1_MIN_FREQ)
    {
      speed_freq = ARDUINO_TONE_TIMER1_MIN_FREQ;
    }

    //Show minutes on tacho
    rpm_freq = round((0.0008 * minutes_numeric * minutes_numeric) + (3.1282 * minutes_numeric) + 4.2545);  //E36 318i
    if(rpm_freq < ARDUINO_TONE_TIMER0_MIN_FREQ)
    {
      rpm_freq = ARDUINO_TONE_TIMER0_MIN_FREQ;
    }

    
    
    Serial.print("Speed freq: ");
    Serial.print(speed_freq);
    Serial.print(" --> (int): ");
    Serial.print((int)speed_freq);
    Serial.print(" --> rounded: ");
    Serial.print(round(speed_freq));
    Serial.println();
    Serial.print("RPM freq: ");
    Serial.print(rpm_freq);
    Serial.print(" --> (int): ");
    Serial.print((int)rpm_freq);
    Serial.print(" --> rounded: ");
    Serial.print(round(rpm_freq));
    Serial.println();
    Serial.println();


    mpg_tone.play(32);
    speed_tone.play(speed_freq);
    rpm_tone.play(rpm_freq);


    //analogWrite(FUEL_PIN, 50);
    //analogWrite(TEMP_PIN, 30);
  }



  //Handle buttons
  if(hour_btn_pressed == false && digitalRead(HOUR_BTN) == HIGH)
  {
    hour_btn_pressed = true;
    incHours();
  }
  if(hour_btn_pressed == true && digitalRead(HOUR_BTN) == LOW)
  {
    hour_btn_pressed = false;
  }

  if(minute_btn_pressed == false && digitalRead(MINUTE_BTN) == HIGH)
  {
    minute_btn_pressed = true;
    incMinutes();
  }
  if(minute_btn_pressed == true && digitalRead(MINUTE_BTN) == LOW)
  {
    minute_btn_pressed = false;
  }
}
