# README #

This project is a wall clock done using Arduino Mega2560 and instrument cluster from BMW E34.  
Speedometer is used to show current hour (X km/h divided by 10 == hour, i.e. 130 km/h means it is 13 hour).  
RPM gauge is used to show current minutes (X RPM multiplied by 10 == minutes, i.e. 3500 RPM means 35 minutes).  

### Harware needed ###

* E34 instrument cluster (diesel is the best because it has speedo up to 240 km/h and RPM up to 6000, but anything more will be ok, it just won't use full scale)
* Arduino Mega2560 or Mega1280 - Uno or Nano will not be enough because they only have one 16-bit timer while we need one 16-bit timer per gauge
* Two pushbuttons
* Two resistors
* Wires
* Breadboard

### Libraries ###

This project uses one external library - Tone.h by Brett Hagman: https://github.com/bhagman/Tone
It does not support Mega2560 by default, so if you want to use it, go to Documents/Arduino/libraries/Tone-1.0.0 and modify Tone.cpp. Replace every occurence of "#if defined(_____AVR_ATmega1280_____)" with "#if defined(_____AVR_ATmega1280_____) || defined(_____AVR_ATmega2560_____)" to treat 2560 just like 1280.  
Without that, every unsupported board type is treated like default ATmega328 (Uno) so we will have only two timers available (0 should not be used!!!)  

Timers order on Mega is: 2, 3, 4, 5, 1, 0  
To assign pin to specific timer, just define your pins like:  
`Tone mpg_tone;`  
`Tone rpm_tone;`  
`Tone speed_tone;`  
And then just call begin in proper order (timers are assigned in sequence):  
`mpg_tone.begin(MPG_PIN);  //Timer2 used`  
`rpm_tone.begin(RPM_PIN);  //Timer 3 used`  
`speed_tone.begin(SPEED_PIN);  //Timer 4 used`  